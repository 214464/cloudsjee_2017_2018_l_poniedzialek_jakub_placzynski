package json;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import java.util.*;
 
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
 
public class Json 
{
 
  public static class TokenizerMapper
  //key in value in key out value out
       extends Mapper<Object, Text, Text, DoubleWritable>
  {
    public void map(Object key, Text value, Reducer.Context context
                    ) throws IOException, InterruptedException
    {
        String line = value.toString();
                String[] tuple = line.split("\\n");
                for(int i=0;i<tuple.length; i++)
                {
                    JsonObject obj = new JsonParser().parse(tuple[i]).getAsJsonObject();
                    String side = obj.get("side").getAsString();
                    String id = obj.get("series").getAsString();
                    String sample = obj.get("sample").getAsString();
 
                        double name = (obj.get("features2D")).getAsJsonObject().get("first").getAsDouble();
                        String kej = id +"_"+ side + "_first" + " ";
                        context.write(new Text(kej), new DoubleWritable(name));
                       
                        name = (obj.get("features2D")).getAsJsonObject().get("second").getAsDouble();                
                        kej = id +"_"+ side + "_second" + " ";
                        context.write(new Text(kej), new DoubleWritable(name));
                       
                        name = (obj.get("features2D")).getAsJsonObject().get("third").getAsDouble();                  
                        kej = id +"_"+ side + "_third" + " ";
                        context.write(new Text(kej), new DoubleWritable(name));
                       
                        name = (obj.get("features2D")).getAsJsonObject().get("fourth").getAsDouble();                
                        kej = id +"_"+ side + "_fourth" + " ";
                        context.write(new Text(kej), new DoubleWritable(name));
                       
                        name = (obj.get("features2D")).getAsJsonObject().get("fifth").getAsDouble();                  
                        kej = id +"_"+ side + "_fifth" + " ";
                        context.write(new Text(kej), new DoubleWritable(name));                  
                }
    }
}
 
  public static class IntSumReducer
       extends Reducer<Text, DoubleWritable, Text, DoubleWritable> 
  {
    private DoubleWritable result = new DoubleWritable();
 
    public void reduce(Text key, Iterable<DoubleWritable> values,
                       Reducer.Context context
                       ) throws IOException, InterruptedException 
    {
        double sum = 0;
        double mean = 0;
        double sigma = 0;
        ArrayList<Double> values2 = new ArrayList<Double>();
        for (DoubleWritable val : values) 
        {
            sum += val.get();
            values2.add(val.get());
        }
        mean = sum / 20.0;
        for(Double val : values2)
        {
            sigma += pow(val - mean, 2.0);
        }
        sigma = sqrt(sigma/20.0);
        result.set(sigma);
        context.write(key, result);
    }
  }
 
  public static void main(String[] args) throws Exception 
  {
    Configuration conf = new Configuration();
    Job job = Job.getInstance(conf, "json_2");
    job.setJarByClass(Json.class);
    job.setMapperClass(TokenizerMapper.class);
    job.setCombinerClass(IntSumReducer.class);
    job.setReducerClass(IntSumReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(DoubleWritable.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}