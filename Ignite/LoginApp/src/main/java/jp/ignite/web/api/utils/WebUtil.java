package jp.ignite.web.api.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import jp.ignite.model.User;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;


public class WebUtil {
    

     static Ignite ignite = Ignition.ignite();
     static IgniteCache<String, String> cache = ignite.getOrCreateCache("cache");    
     static IgniteCache<String, User> userCache = ignite.getOrCreateCache("cache");    
    
    public static String getHTML(String urlToRead) throws Exception {
      StringBuilder result = new StringBuilder();
      URL url = new URL(urlToRead);
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setRequestMethod("GET");
      BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
      String line;
      while ((line = rd.readLine()) != null) {
         result.append(line);
      }
      rd.close();
      return result.toString();
   }

    public static  boolean isLogged()
    {
        if(userCache.containsKey("user"))
            return true;
        return false;
    }    
    
    public static User getUserFromIgnite()
    {
        return userCache.get("user");
    }    
    
    public static void putUserToIgnite(User user)
    {
        userCache.put("user", user);
    }     
    
    public static void cleanUserCache()
    {
        userCache.clear("user");
    }
    
    public static  boolean isCache()
    {
        if(cache.containsKey("result"))
            return true;
        return false;
    }    
    
    public  static String getFromIgnite()
    {
        return cache.get("result");
    }
    public static  String downloadHadoopOutput()  
    {
    try {
        String result = getHTML("http://192.168.5.11:50075/webhdfs/v1/user/vagrant/output/part-r-00000?op=OPEN&offset=0&user.name=vagrant&namenoderpcaddress=hadoop-master:8020");
        cache.put("result","pobrano z Ignite: " + result);
        return "pobrano z HTML: " + result;
    } catch (Exception ex) {
        Logger.getLogger(WebUtil.class.getName()).log(Level.SEVERE, null, ex);
    }
    return "very null";
                
    }

}
