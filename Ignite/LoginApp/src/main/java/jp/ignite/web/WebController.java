package jp.ignite.web;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jp.ignite.model.User;
import jp.ignite.model.UserList;
import jp.ignite.web.api.utils.WebUtil;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;

@Controller
public class WebController {

    private static final String USER_SESSION_VALUE = "user";
    private final UserList userList;


    @Autowired
    public WebController(UserList userList) {
        this.userList = userList;
    }

    @GetMapping("/login")
    public String loginGet(HttpServletRequest request, Model model) {
        Object user = request.getSession().getAttribute(USER_SESSION_VALUE);
        if (user != null) {
            model.addAttribute("user", user);
            if (WebUtil.isCache())
                    {model.addAttribute("hadoop", WebUtil.getFromIgnite());}
                else
                    {model.addAttribute("hadoop", WebUtil.downloadHadoopOutput());}
                return "getResult";
        }
        else if (WebUtil.isLogged())
        {
            user = WebUtil.getUserFromIgnite();
            model.addAttribute("user",user);
            if (WebUtil.isCache())
                    {model.addAttribute("hadoop", WebUtil.getFromIgnite());}
                else
                    {model.addAttribute("hadoop", WebUtil.downloadHadoopOutput());}
                return "getResult";
        }
        return "login";
    }

    @PostMapping("/login")
    public String loginPost(@RequestParam("login") String username, @RequestParam("password") String password,
            HttpServletRequest request, Model model) {
        Optional<User> findOne = userList.findOne(username);
        if (findOne.isPresent()) {
            User user = findOne.get();
            if (user.getPassword().equals(password)) {
                WebUtil.putUserToIgnite(user);
                request.getSession().setAttribute(USER_SESSION_VALUE, user);
                model.addAttribute("user", findOne.get());
                if (WebUtil.isCache())
                    {model.addAttribute("hadoop", WebUtil.getFromIgnite());}
                else
                    {model.addAttribute("hadoop", WebUtil.downloadHadoopOutput());}
                return "getResult";
            }
        }
        model.addAttribute("invalidCredentials", true);

        return "login";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request) {
        request.getSession().removeAttribute(USER_SESSION_VALUE);
        WebUtil.cleanUserCache();
        return "redirect:/login";
    }
}