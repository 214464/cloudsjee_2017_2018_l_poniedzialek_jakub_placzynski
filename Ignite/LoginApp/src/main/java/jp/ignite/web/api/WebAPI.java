package jp.ignite.web.api;

import retrofit2.Call;
import retrofit2.http.GET;

public interface WebAPI {

        @GET("http://192.168.5.11:50075/webhdfs/v1/user/vagrant/output/part-r-00000?op=OPEN&offset=0&user.name=vagrant&namenoderpcaddress=hadoop-master:8020")
        Call<String> getHadoopOutput();

}
