package jp.ignite.config;

import javax.servlet.ServletContextListener;

import org.apache.ignite.cache.websession.WebSessionFilter;
import org.apache.ignite.startup.servlet.ServletContextListenerStartup;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    public static String IGNITE = "ignite-config.xml";
    public static String CACHE = "cache";

    @Bean
    public ServletContextListener servletContextListener() {
        return new ServletContextListenerStartup();
    }

    @Bean
    public ServletContextInitializer servletContextInitializer() {
        return servletContext -> {
            servletContext.setInitParameter("IgniteConfigurationFilePath", IGNITE);
            servletContext.setInitParameter("IgniteWebSessionsCacheName", CACHE);
        };
    }

    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setName("IgniteWebSessionsFilter");
        filterRegistrationBean.setFilter(new WebSessionFilter());
        filterRegistrationBean.addUrlPatterns("/*");
        return filterRegistrationBean;
    }

}