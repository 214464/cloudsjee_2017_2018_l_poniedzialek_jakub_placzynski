package jp.ignite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.system.ApplicationPidFileWriter;

@SpringBootApplication
public class Main {

	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication(Main.class);
		springApplication.addListeners(new ApplicationPidFileWriter("pid_boot_app"));
		springApplication.run(args);
	}
}
